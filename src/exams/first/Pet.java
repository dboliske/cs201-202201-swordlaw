package exams.first;

import labs.lab4.Potion;

public class Pet {
		
		private String name;
		private int age;
		
		Pet(){
			this.name = "undefined";
			this.age = 0;
		}
		
		Pet(String name, int age){
			this.name = name;
			this.age = age;
		}
		public String setName(String name) {
			this.name = name;
		}
		public int setAge(int age) {
			this.age = age;
		}
		public String getName() {
			return name;
		}
		public int getAge() {
			return age;
		}
		public boolean equals(Object other) {
			return String.format("Name: " + name + " Age: " + age) != null;
		}
		
		@Override
		public String toString() {
			return String.format("Name: " + this.name +  " Age: " + this.age);
		}
	}
}




