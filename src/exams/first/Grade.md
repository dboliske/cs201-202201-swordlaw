# Midterm Exam

## Total

75/100

## Break Down

1. Data Types:                  15/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               0/5
    - Results:                  5/5
2. Selection:                   17/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  2/5
3. Repetition:                  17/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               2/5
4. Arrays:                      15/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     11/20
    - Variables:                5/5
    - Constructors:             3/5
    - Accessors and Mutators:   1/5
    - toString and equals:      2/5

## Comments

1. Doesn't convert result to a character.
2. Doesn't work if the number is divisible by both 2 and 3.
3. Doesn't print a triangle.
4. Reads everything in correct, but doesn't look for repeats.
5. There is an extra bracket at the end of your class, the non-default constructor and mutator do not validate `age`, the mutators do not have a void return type, and the `equals` method does not correctly compare instance variables.
