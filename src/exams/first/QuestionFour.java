package exams.first;

import java.util.*;

public class QuestionFour {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//5 word array
        String[] words = new String[5];

        //enter a word prompt
        for (int i = 0; i < words.length; i++) {
            System.out.println("Enter a word:");
            words[i] = input.nextLine();
        } 
        
        //if the words are matching (if/else statement) 
        if (words == words) {
        	System.out.println(words + "Are the Same");
        } else {
        	System.out.println("No Matching Words");
        }

        input.close();
    }
}
