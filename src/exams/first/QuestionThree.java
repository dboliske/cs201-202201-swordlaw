package exams.first;

import java.util.*;

public class QuestionThree {

	public static void main(String[] args) {
		//new scanner
		Scanner input = new Scanner(System.in);
		int num;
		char c;
		//enter length
		System.out.print (" Please enter the length of the side: ");
		num = input.nextInt();
		//choose char
		System.out.print (" Pick a character for the triangle to be made of: ");
		c=input.next().charAt(0);
		
		//this is still the square, mind is blanking for triangle
		for (int x=num;x>=1;--x) {
		    for (int y=num;y>=1;y--) {
		        System.out.print (c);
		    }
		    System.out.println ();
		    }
		}
	}
