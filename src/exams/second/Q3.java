package exams.second;
import java.util.*;

public class Q3 {

	public static void main(String[] args) {
		ArrayList<Integer> nums = new ArrayList<Integer>();
		Scanner input = new Scanner(System.in);
		
		String num = " "; 
		while(true) {
		System.out.print("Please Enter a Number: ");
		num = input.nextLine();
		if (num.equals("exit"))
			break;
		nums.add(Integer.parseInt(num));
		}
		Integer max = Collections.max(nums);
		Integer min = Collections.min(nums);
		System.out.println("Max value: " + max);
		System.out.println("Min value: " + min);

	}

}
