package exams.second;

public class Rectangle extends Polygon {
	private double width, height;
	
	
	Rectangle(){
		this.height = 0.0;
		this.width = 0.0;
	}
	
	public void setWidth(double width) {
		this.width = width; 
	}
	public void setHeight(double height) {
		this.height = height; 
	}
	public double getWidth() {
		return width;
	}
	public double getHeight() {
		return height;
	}
	@Override 
	public String toString() {
		return "(" + height + ":" + width + ")";
	}
	public double area(){
		return height * width; 

	}
	public double perimeter() {
		return (2.0 * (height + width));
	}

}
