package exams.second;

public class Q4 {

	  public static void selectionSort(String[] arr){  
	        for (int i = 0; i < arr.length - 1; i++)  
	        {  
	            int index = i;  
	            String minStr = arr[i];
	            for (int j = i + 1; j < arr.length; j++){  
	                if (arr[j].compareTo(minStr) < 0){ 
	                	minStr= arr[j];
	                    index = j;//searching for lowest index  
	                }  
	            }  
	            if(index != 1) {
	            	String temp = arr[index];
	            	arr[index] = arr[i];
	            	arr[i] = temp;
	            }
	              
	        }  
	    }  
	       
	    public static void main(String a[]){  
	        String[] arr1 = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};  
	        System.out.println("Before Selection Sort");  
	        for(String i:arr1){  
	            System.out.print(i + " ");  
	        }  
	        System.out.println();  
	          
	        selectionSort(arr1);//sorting array using selection sort  
	         
	        System.out.println("After Selection Sort");  
	        for(String i:arr1){  
	            System.out.print(i + " ");  
	        }  
	    }  
	} 