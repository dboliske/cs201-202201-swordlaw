package exams.second;

import java.util.Scanner;

public class Q5 {
	
	public static int jumpSearchArr(double[] arraySearch, double element) {
	    int blockScrh = (int) Math.sqrt(arraySearch.length);
	    int currentLastIndex = blockScrh-1;
	    return jumpSearch(arraySearch, element, blockScrh, currentLastIndex);
	}

	public static int jumpSearch(double[] arraySearch, double element, int blockScrh, int currentLastIndex) {
	    if (currentLastIndex < arraySearch.length && element > arraySearch[currentLastIndex]) {
	        currentLastIndex += blockScrh;
	        // recursive
	        return jumpSearch(arraySearch, element, blockScrh, currentLastIndex);
	    } 
	    else {
	        // linear search
	        for (int currentSearchIndex = currentLastIndex - blockScrh + 1;currentSearchIndex <= currentLastIndex && currentSearchIndex < arraySearch.length; currentSearchIndex++) {
	            if (element == arraySearch[currentSearchIndex]) {
	                return currentSearchIndex;
	            }
	        }
	    }
	    // element not found. Return -1
	    return -1;
	}
	
	public static void main(String a[]) {
        double[] arr1 = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        System.out.print("What value would you like to search? ");
		Scanner input = new Scanner(System.in);
		double val = input.nextDouble();
		//print element position
		System.out.print(jumpSearchArr(arr1, val));
	
	}
}

