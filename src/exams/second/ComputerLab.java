package exams.second;

public class ComputerLab extends Classroom {
	private boolean computers;
	
	ComputerLab(){
		super();
	}
	
	public boolean setComputers(boolean computers) {
		return computers;
	}
	
	public boolean hasComputers() {
		return computers;
		
	}
	@Override
	public String toString() {
		return ("The Computer lab has: " + computers );
	}

}
