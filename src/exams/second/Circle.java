package exams.second;

public class Circle extends Polygon{
	private double radius;
	
	Circle(){
		this.setRadius(0.0);
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}


	@Override
	public String toString() {
		return (name);
	}

	public double area() {
		return (Math.PI * radius * radius);
		
	}

	public double perimeter() {
		return (2.0 * Math.PI * radius);		
	}


}
