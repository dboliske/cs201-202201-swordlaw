package exams.second;

public abstract class Polygon {
	protected String name;
	
	public Polygon() {
		this.name = " ";
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public abstract String toString();
	public abstract double area();
	public abstract double perimeter();
	

}
