package exams.second;

public class Classroom {
	protected String building, roomNumber;
	private int seats;
	
	Classroom(){
		this.building = " ";
		this.roomNumber = " ";
		this.seats = 0;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public String getBuiding() {
		return building;
	}
	public String getRoomNumber() {
		return roomNumber;
	}
	public int getSeats() {
		return seats;
	}
	@Override
	public String toString() {
		return ("The Classroom is locatated: " + building + " " + roomNumber + " with " + seats + " seating");
	}

}
