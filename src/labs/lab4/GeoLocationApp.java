package labs.lab4;

public class GeoLocationApp {
	
	public static void main(String[] args) {
		//default
		GeoLocation de = new GeoLocation();
		System.out.println(de.getLat() + ", " + de.getLng());
		
	    //non-default
		GeoLocation ne = new GeoLocation(30, 40);
		System.out.println(ne.getLat() + ", " + ne.getLng());
	}

}
