package labs.lab4;

public class Potion {
	
	private String name;
	private double strength; 
	
	Potion() {
		this.name = "undefined";
		this.strength = 0.0;
		
	}
	
	Potion(String name, double strength) {
		this.name = name;
		this.strength = strength; 
	}
	
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	 public void setName(String name) {
		 this.name = name;
	}
	public void setStrength(double strength) {
		this.strength = strength;
	}
	public boolean validStrength() {
		if (strength >= 0 && strength <= 10) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(Potion other) {
		return other.getName().equals(this.getName()) && other.getStrength() == this.getStrength();
	}
	
	@Override
	public String toString() {
		return String.format("Name: " + this.name +  " Strength: " + this.strength);
	}
	

}
