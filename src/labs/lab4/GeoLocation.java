package labs.lab4;

public class GeoLocation {
	private double lat, lng;

    GeoLocation() {
        this.lat = 0.0;
        this.lng = 0.0;
    }

    GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public boolean validLat() {
        if(this.lat < -90) {
            return false;
        }
        else if(this.lat > 90) {
            return false;
        }
        return true;
    }

    public boolean validLng() {
        if(this.lng < -180) {
            return false;
        }
        else if(this.lng > 180) {
            return false;
        }
        return true;
    }
    
    public boolean equals(GeoLocation l) {
    	return l.getLat()==(this.getLat()) && l.getLng()==(this.getLng());
    }

    @Override
    public String toString() {
        return "(" + lat + ", " + lng + ")";
    }
}
