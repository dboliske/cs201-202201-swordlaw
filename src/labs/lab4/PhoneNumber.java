package labs.lab4;

public class PhoneNumber {
	
	private String countryCode, areaCode, number;

    PhoneNumber() {
        this.countryCode = "1";
        this.areaCode = "555";
        this.number = "5555555";
    }

    PhoneNumber(String countryCode, String areaCode, String number) {
        this.countryCode = countryCode;
        this.areaCode = areaCode;
        this.number = number;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public boolean validAreaCode() {
        return this.areaCode.length() == 3;
    }

    public boolean validNumber() {
        return this.number.length() == 7;
    }
    
    public boolean equals(PhoneNumber pn) {
    	return pn.getCountryCode().equals(this.getCountryCode()) && pn.getAreaCode().equals(this.getAreaCode()) && pn.getNumber().equals(this.getNumber());
    }

    @Override
    public String toString() {
        return String.format("+%s (%s) %s", this.countryCode, this.areaCode, this.number);
    }
}
