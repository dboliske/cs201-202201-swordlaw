package labs.lab4;

public class PotionApp {

	public static void main(String[] args) {
		//default
		Potion de = new Potion();
		System.out.println(de.toString());
		
		//non-default
		Potion ne = new Potion("Health", 100);
		System.out.println(ne.toString());
	}

}
