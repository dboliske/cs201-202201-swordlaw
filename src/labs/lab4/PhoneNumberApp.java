package labs.lab4;

public class PhoneNumberApp {

	public static void main(String[] args) {
		//default
		PhoneNumber de = new PhoneNumber();
		System.out.println(de.toString());
		
		//non-default
		PhoneNumber ne = new PhoneNumber("1", "312", "1234567");
		System.out.println(ne.toString());
		
	}

}
