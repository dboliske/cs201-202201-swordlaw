package labs.lab2;

import java.util.*;

public class lab2Ex3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
        // variables to store inputs and option menu 
        int option, x, y;
        System.out.print("MENU OPTIONS:\n1 - Say Hello\n2 - Addition\n3 - Multiplication\n4 - Exit Program\n\nEnter selection(1-4): ");
        // switch cases for options beside exit
        while((option = input.nextInt()) != 4) {
            switch(option) {
                case 1:
                    System.out.println("Hello");
                    break;
                    // addition
                case 2:
                    System.out.print("Enter the first number to add: ");
                    x = input.nextInt();
                    System.out.print("Enter the second number to add: ");
                    y = input.nextInt();
                    System.out.println(x + " + " + y + " = " + (x + y));
                    break;
                    // multiplication 
                case 3:
                    System.out.print("Enter the first number to multiply: ");
                    x = input.nextInt();
                    System.out.print("Enter the second number to multiply: ");
                    y = input.nextInt();
                    System.out.println(x + " * " + y + " = " + (x * y));
                    break;
                    // if invalid input
                default:
                    System.out.println("Invalid option!");
            }
            //loop back to menu options
            System.out.print("MENU OPTIONS:\n1 - Say Hello\n2 - Addition\n3 - Multiplication\n4 - Exit Program\n\nEnter selection(1-4): ");
        }
    }
}

