package labs.lab2;

import java.util.*;

public class lab2Ex1 {
	

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//variables
		int num;
		//char for the character used to the make square 
		char c;
		System.out.print (" Please enter the number of rows for the square: ");
		num = input.nextInt();
		System.out.print (" Pick a character for the square to be made of: ");
		c=input.next().charAt(0);
		//for loop
		for (int x=num;x>=1;--x) {
		    for (int y=num;y>=1;y--) {
		        System.out.print (c);
		    }
		    System.out.println ();
		}

	}

}
