package labs.lab2;

import java.util.*;

public class lab2Ex2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int total = 0, n, count = 0;
		// while loop for grade entering
		while  (true) {
			System.out.print("Enter a grade (-1 to finish entering): ");
			n = input.nextInt();
			// to exit the loop
			if (n == -1) {
				break;
				}
			total += n;
			count++;
			}
		// calculate the average
		float average = total / (count + 1);
		System.out.println("Average exam grade is: " + average);
		}
	} 