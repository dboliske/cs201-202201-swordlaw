package labs.lab1;

import java.util.Scanner;

public class exerciseOne {

	public static void main(String[] args) {
		// crate input variable 
		Scanner input = new Scanner(System.in);
		// prompt user for input
		System.out.print("Please enter your name: ");
		//store input to variable 
		String userName = input.nextLine();
		//print stored name
		System.out.println("Hello " + userName);

	}

}
