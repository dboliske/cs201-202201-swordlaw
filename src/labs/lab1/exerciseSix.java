package labs.lab1;

import java.util.Scanner;

public class exerciseSix {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		//prompt for input
		System.out.print("Enter a length in Inches: ");
		double inch = input.nextDouble();
		
		//convert in to cm
		System.out.println(inch + " inches converted to centimeters is: " + (inch * 2.54));

	}

}
