package labs.lab1;

import java.util.Scanner;

public class exerciseFive {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Please enter the length(in.) of the box: ");
		double length = input.nextDouble();
		System.out.print("Please enter the width(in.) of the box: ");
		double width = input.nextDouble();
		System.out.print("Please enter the depth(in.) of the box: ");
		double depth = input.nextDouble();
		
		double sufarea = (2 * (length * width + length * depth + width * depth) / 12);
		double vol = (length * width * depth);
		
		System.out.println("The surface area (Material needed) is equal to: " + sufarea + " squared feet, with a volume of: " + vol + " cubed inches");


	}

}
