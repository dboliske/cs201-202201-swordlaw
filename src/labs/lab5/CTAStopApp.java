package labs.lab5;

import java.io.*;
import java.util.*;
import java.io.IOException;

public class CTAStopApp {

	public static void main(String[] args) throws IOException {
		CTAStation[] stations= new CTAStation[64];
		FileReader file = new FileReader("src/labs/lab5/CTAStops.csv");
		Scanner scanner = new Scanner(file);
		Scanner console = new Scanner(System.in);
		
        // Read CSV file 
        int stn = -1;
        while(scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if(stn != -1) {
                String[] parts = line.split(",");
                stations[stn] = new CTAStation(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]), parts[0], parts[3], Boolean.parseBoolean(parts[4]), Boolean.parseBoolean(parts[5]));
            }
            stn++;
        }
        file.close();
        
     // Menu
        int selection;
        System.out.print("OPTIONS:\n1 - Display Station Names\n2 - Display Stations w/ or w/o Wheelchair Access\n3 - Display Nearest Station\n4 - Exit\n\nEnter selection: ");
        //If it's 4 exit, else proceed into the loop.
        while((selection = Integer.parseInt(console.nextLine())) != 4) {
            switch(selection) {
            	//Display Names
                case 1:
                    for(int i = 0; i < stations.length; i++) {
                        if(stations[i] != null)
                            System.out.println(stations[i].getName());
                    }
                    break;
                //Display w/o access
                case 2:
                	boolean accessible;
                	System.out.println("Accessiblity? Please enter (y/n)");
                	String answer = console.nextLine().toLowerCase();
                	if(answer.equals("y")) {
                		accessible = true;
                	} else {
                		accessible = false;
                	}
                    
                    ArrayList<CTAStation> filtered = new ArrayList<CTAStation>();
                    for(int i = 0; i < stations.length; i++) {
                        if(stations[i] != null && stations[i].isWheelchair() == accessible) {
                            filtered.add(stations[i]);
                        }
                    }
                    if(filtered.size() != 0) {
                        for (CTAStation station : filtered) {
                            System.out.println(station.getName());
                        }
                    } else {
                        System.out.println("No CTA stations matching your parameters were found.");
                    }
                    break;
                //Display Closest Station    
                case 3:
                	System.out.print("Latitude: ");
                	double lat = Double.parseDouble(console.nextLine());
                	System.out.print("Longtitude: ");
                	double lng = Double.parseDouble(console.nextLine());	
                    GeoLocation point = new GeoLocation(lat, lng);
                    int minimum = 0;
                    for(int i = 0; i < stations.length; i++) {
                        if(stations[i] != null && stations[i].calcDistance(point) < stations[minimum].calcDistance(point)) {
                            minimum = i;
                        }
                    }
                    System.out.println("Closest station is: " + stations[minimum].getName());
                    break;
                default:
                    System.out.println("Invalid option!");
            }
            // Print menu again for the loop
            System.out.print("\nOPTIONS:\n1 - Display Station Names\n2 - Display Stations w/ or w/o Wheelchair Access\n3 - Display Nearest Station\n4 - Exit\n\nEnter selection: ");
        }

	}

}
