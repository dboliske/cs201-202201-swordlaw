package labs.lab5;

import java.util.*;

public class CTAStation extends GeoLocation {
    private String name;
    private String location;
    private boolean open;
    private boolean wheelchair;

    CTAStation(double lat, double lng, String name, String location, boolean wheelchair, boolean open) {
        this.setLat(lat);
        this.setLng(lng);
        this.name = name;
        this.location = location;
        this.wheelchair = wheelchair;
        this.open = open;
    }
    
    public String getName() {
    	return name;
    }
    
    public void setName(String name) {
    	this.name = name;
    }
    
    public String getLocation() {
    	return location;
    }
    
    public void setLocation(String location) {
    	this.location = location;
    }
    
    public boolean isOpen() {
    	return open;
    }
    
    public void setOpen(boolean open) {
    	this.open = open;
    }
    
    public boolean isWheelchair() {
    	return wheelchair;
    }
    
    public void setWheelchair(boolean wheelchair) {
    	this.wheelchair = wheelchair;
    }
   
    
    @Override
    public String toString() {
    	return name + " " + super.toString() + "is a " + location + " Cta Station that is " + 
    (this.wheelchair ? "wheelchair accessible" : "not wheelchair accessible") + " and is " + (this.open ? "Open" : "Closed");
    }
    
    @Override
    public boolean equals(Object o) {
    	if (this == o)
    		return true;
    	if (o == null || getClass() != o.getClass())
    		return false;
    	CTAStation that = (CTAStation) o;
    	return super.equals(o) && open == that.open 
    			&& wheelchair == that.wheelchair 
    			&& Objects.equals(name, that.name) 
    			&& Objects.equals(location, that.location);
    }

}
