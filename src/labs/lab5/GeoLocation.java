package labs.lab5;

public class GeoLocation {
	private double lat, lng;

    GeoLocation() {
        this.lat = 0.0;
        this.lng = 0.0;
    }

    GeoLocation(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public boolean validLat() {
        if(this.lat < -90) {
            return false;
        }
        else if(this.lat > 90) {
            return false;
        }
        return true;
    }

    public boolean validLng() {
        if(this.lng < -180) {
            return false;
        }
        else if(this.lng > 180) {
            return false;
        }
        return true;
    }
    
    public boolean equals(GeoLocation l) {
    	return l.getLat()==(this.getLat()) && l.getLng()==(this.getLng());
    }
    
    public double calcDistance(GeoLocation otherLocation) {
        return Math.sqrt(Math.pow(this.lat-otherLocation.getLat(),2)+Math.pow(this.lng-otherLocation.getLng(),2));
    }
    
    public double calcDistance(double otherLat, double otherLng) {
        return Math.sqrt(Math.pow(this.lat-otherLat,2)+Math.pow(this.lng-otherLng,2));
    }
    
    public static GeoLocation fromString(String source) {
        String[] parts = source.split(",");
        return new GeoLocation(Double.parseDouble(parts[0].replaceAll("[()]", "")), Double.parseDouble(parts[1].replaceAll("[()]", "")));
    }

    @Override
    public String toString() {
        return "(" + lat + ", " + lng + ")";
    }
}
