package labs.lab7;

import java.util.Scanner;
public class BinarySearch {
	static int binarySearch(String[] arr, String x) {
		int l = 0, r = arr.length - 1;
		while (l <= r) {
			int m = l + (r - l) / 2;
			int res = x.compareTo(arr[m]);
			
			if (res == 0)   
				return m;
			if (res > 0) 
				l = m + 1;
			else    
				r = m - 1;
			}
		return -1; //element does not exist
		}
	
	public static void main(String []args) {
		String[] arr = { "c", "html", "java", "python", "ruby", "scala"};
		
		//scanner for user input
		Scanner input = new Scanner(System.in);
		System.out.print("Enter a string: ");
		String str = input.nextLine(); 
		
		int result = binarySearch(arr, str);
		if (result == -1)
			System.out.println("Element Does Not Exist");
		else
			System.out.println("Element found at index: " + result);
		}
	}