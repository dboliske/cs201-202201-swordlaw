package labs.lab7;

public class bubbleSort {
	public static void BubbleSort(int[] arr) {
		int n = arr.length; 
		int temp = 0; 
		for(int i = 0; i < n; i++) 
		{ //loop 
			for(int j=1; j < (n-i); j++) 
			{ 
				if(arr[j-1] > arr[j]) 
				{ //compare adjacent elements
					temp = arr[j-1]; 
					arr[j-1] = arr[j]; //swap position if conditions met
					arr[j] = temp;
					}
				}
			}
		}
	
	public static void main(String[] args) {
		int arr[] = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 };
		System.out.println("Array Before Bubble Sort");

		for(int i = 0; i < arr.length; i++) { //before sort
			System.out.print(arr[i] + " ");
			}
		System.out.println();
		BubbleSort(arr); //sorting
		
		System.out.println("Array After Bubble Sort");
		for(int i = 0; i < arr.length; i++) { //after sort
			System.out.print(arr[i] + " ");
			}
		}
	}
