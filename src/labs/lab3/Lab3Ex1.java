package labs.lab3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Lab3Ex1 {
	
	public static void main(String[] args) {
		String line = "";
		String splitBy = ",";
		int count=0;
		float sum=0;
 		
		try {
			// putting CSV file into BufferedReader 
			BufferedReader br = new BufferedReader(new FileReader("src/labs/lab3/grades.csv"));
			
			while ((line = br.readLine()) != null) {
				// splitting array with ","
				String[] students = line.split(splitBy); 
				int grade = Integer.parseInt(students[1]);
				//sum of grades
				sum += grade;
				//number of grades
				count++; 
				}
			
			//average
			float avg = sum/count; 
			System.out.println("Class Average = " + avg); 
			} 
		
		catch (IOException e) {
			e.printStackTrace();
			}
		}
	}

