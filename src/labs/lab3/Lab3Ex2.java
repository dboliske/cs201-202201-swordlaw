package labs.lab3;

import java.util.*;
import java.io.*;

public class Lab3Ex2 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		int number;
		String numStr,str = "";
		
		try {
			while(true) {
				//ask user for input
				System.out.print("\nEnter a number: ");
				numStr = input.next();
				//to exit input "Done"
				if(numStr.equalsIgnoreCase("Done")) { // ignore case sensitivity 
					break;
					}
				number = Integer.parseInt(numStr);
				str = str + number + "\n";
				}
			
			System.out.print("\nEnter a file name with extension: ");
			//filename storage
			String fileName = input.next();
			FileWriter fw = new FileWriter(fileName);
			fw.write(str);
			System.out.println("\nStored successfully!!\n");
			fw.close();
			}
		//for exceptions
		catch(Exception e1) {
			System.out.println("Exception is "+e1);
			}
		}
	}