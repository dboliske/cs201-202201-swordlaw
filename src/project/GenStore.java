//Simeon Wordlaw 
//4/29/22
//final project
package project;

import java.util.*;
import java.io.*;

public class GenStore {

	public static void fileOperations() {
		//try catch so if there is a error with the file being read it wont die.
		//I tried doing as much inside of try/catch
		try {
			Scanner input = new Scanner(System.in);                        
			File myObj = new File("src/project/stock.csv");
			Scanner myReader = new Scanner(myObj);
			LinkedHashMap<ArrayList<String>, Integer> hm = new LinkedHashMap<>();
			while (myReader.hasNextLine()) {
				String data = myReader.nextLine();
				String[] array = data.split(",");
				ArrayList<String> ar = new ArrayList<>();
				for (int i = 0; i < array.length; i++) {
					ar.add(array[i]);
				}
				hm.put(ar, hm.getOrDefault(ar, 0) + 1);
			}
			//options menu
			System.out.println("\nEnter an option: (1-5) "
							+ "\n[1] Add an item "
							+ "\n[2] Sell an item "
							+ "\n[3] Search for an item "
							+ "\n[4] Modify an item "
							+ "\n[5] Exit the store");
			int choice = input.nextInt();
			System.out.println(choice);
			while (choice < 6) {
//				//option 1
				if (choice == 1) {
					System.out.println("Enter an item separated by commas. Ex bread,1.25,4/20/2022");
					input.nextLine();
					String item = input.nextLine();
					//split items with commas for csv file
					String[] array = item.split(",");
					ArrayList<String> ar = new ArrayList<>();
					for (int i = 0; i < array.length; i++) {
						ar.add(array[i]);
					}
					System.out.println("Enter Quantity of item: ");
					int quantity = input.nextInt();
					hm.put(ar, quantity);
					try {
						FileWriter fr = new FileWriter(myObj, true);
						BufferedWriter br = new BufferedWriter(fr);
						while (quantity-- > 0) {
							br.write("\n" + item);
						}
						System.out.println("Item added to file successfully...");
						br.close();
					}
					catch (IOException e) {
						System.out.print(e.getMessage());
					}
					fileOperations();
				}
//				//option 2
				else if (choice == 2) {
					double total = 0;
					System.out.println("Enter item to be sold");
					String sellItem = input.next();
					System.out.println("Enter quantity you wanna sell :");
					int quantity = input.nextInt();
					int quantityAvailable = 0;
					double price = 0;
					ArrayList<String> temp = new ArrayList<>();
					for (Map.Entry<ArrayList<String>, Integer> map : hm.entrySet()) {
						temp = (ArrayList<String>) map.getKey();
						if (temp.get(0).equals(sellItem)) {
							quantityAvailable = map.getValue();
							price = Double.valueOf(temp.get(1));
							fileOperations();
						}
					}
					if (quantity > quantityAvailable) {
						System.out.println("This is more than is in stock");
					} 
					else {
						hm.put(temp, quantityAvailable - quantity);
					}
					total = quantityAvailable * price;
					System.out.println("The subtotal is: " + total);
					fileOperations();
				}
//				//option 3
				else if (choice == 3) {
					System.out.println("Enter an item to search in the store : ");
					String search = input.next();
					ArrayList<String> temp = new ArrayList<>();
					boolean found = false;
					for (Map.Entry<ArrayList<String>, Integer> map : hm.entrySet()) {
						temp = (ArrayList<String>) map.getKey();
						if (temp.get(0).equals(search)) {
							found = true;                                                      
						}
					}
					System.out.println(found ? "Item Found" : "No item found");
					fileOperations();
				}
//				//option 4
				else if (choice == 4) {
					System.out.println("Choose an option: [1] Modify Price [2] Modify Name");
					int internalChoice = input.nextInt();
					if(internalChoice == 1) {
						System.out.println("Enter the item to be modified : ");
						input.nextLine();
						String name = input.next();
						System.out.println("Enter the modified price");
						double modifiedPrice = input.nextDouble();
						ArrayList<String> temp = new ArrayList<>();
						for (Map.Entry<ArrayList<String>, Integer> map : hm.entrySet()) {
							temp = (ArrayList<String>) map.getKey();
							if(temp.get(0).equals(name)) {
								temp.add(1 , Double.toString(modifiedPrice));
								fileOperations();
							}
						}
						System.out.println("Price Updated");
					}
					else {
						System.out.println("Enter the item to be modified : ");
						input.nextLine();
						String name = input.next();
						ArrayList<String> temp = new ArrayList<>();
						for (Map.Entry<ArrayList<String>, Integer> map : hm.entrySet()) {
							temp = (ArrayList<String>) map.getKey();
							if(temp.get(0).equals(name)) {
								temp.add(0 , name);
								fileOperations();
							}
						}
						System.out.println("Name Updated");
					}
					fileOperations();
				}
//				//option 5
				else if (choice == 5) {
					System.out.println("Goodbye, have a nice day!");
					break;
				} 
				else {
					System.out.println("Not a Valid Option");
				}
			}
			input.close();
			myReader.close();
		}
		catch (FileNotFoundException e) {
			System.out.println("An error has occurred. File not found");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		System.out.println("Welcome to the General Store!");
		fileOperations();
	}

}